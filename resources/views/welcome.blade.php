<!DOCTYPE html>
<html>
<head>
    <title>Proximamente!</title>
</head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<body style="background-color: rgba(34, 74, 144, 0.58);">

<div class="row" style="margin-top: 100px">

<div class="col-xs-12" style="text-align: center">

<img src="{{asset('/logotec.png')}}" style="width: 100%;max-width: 150px">
    


    
    <h4 style="color: white;
    margin-bottom: 50px;
    font-size: 30px;
    margin-top: 50px;">Nos estamos renovando para ti! <br>
    No dudes en llamarnos o mandarnos un mensaje con tus dudas.
    </h4>



<div class="col-xs-12"> 
    <div class="col-xs-6">
    <div class="plantel">
        
            Calzada Ignacio Zaragoza No. 1329 Col. Tepalcates, C.P. 09210 Del. Iztapalapa, México D.F.
Tel: 57.56.49.38 | 57.63.56.28
Mail: informes@tecdemexico.edu.mx
         Canal de San Juan
    </div>

    </div>


    <div class="col-xs-6">

    <div class="plantel">
        Parma No.3, Esquina Tenorios
Col. Ex-Hacienda de Coapa, CP.14300
Del. Tlalpan, México DF.
Tel. 56.77.99.34 | 56.77.92.09
Mail: informes@tecdmexico.edu.mx
    </div>
        
    </div>
</div>
<button type="button" class="btn btn-info butbut" data-toggle="modal" data-target="#ayuda" style="margin-top: 70px;
    font-size: 23px;">Enviar Mensaje</button>

<!-- Modal -->
<div id="ayuda" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content mod">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">¡Déjanos un mensaje!</h4>
      </div>
      <div class="modal-body">

      


<form style="height: 280px;" method="POST" action="http://formspree.io/informes@tecdemexico.edu.mx">
<div class="col-xs-12" style="text-align: center;">
<p style="margin-bottom:40px;font-size: 20px">  ¿Tienes alguna pregunta?
   ¡Déjanos un mensaje!</p>
   <input type="hidden" name="_subject" value="Nuevo mensaje de usuario" />
   <input type="text" name="_gotcha" style="display:none" />
<input type="email" class="nombreCorreo" name="email" id="ayudaMail" placeholder="Tu Correo"><br/>
  <textarea name="message"  class="mensaje" style="resize:none;"  id="ayudaMensaje" placeholder="Tu Mensaje"></textarea><br/>
  <button class="enviar" type="submit" style="display: none">Enviar</button>
</div>

</form>

  <button class="enviar btn btn-info butbut" onclick="enviar()">Enviar</button>






      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default butbut" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div></div>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<style type="text/css">
    .plantel{
            margin-left: 200px;
    text-align: center;
    width: 100%;
    max-width: 400px;
    font-size: 20px;
    background: white;
    border-radius: 2px;
    padding: 30px;
    box-shadow: 2px 7px 5px rgba(0, 0, 0, 0.32);
    }
    .mod{
        background-color: #224a90;
        color: white
    }
.mensaje{
        text-align: center;
    height: 150px;
    resize: none;
    width: 100%;
}
.nombreCorreo{
        width: 220px;
    margin-bottom: 30px;
    text-align: center;
}
.butbut{
        border: none;
    background: orange;
}

#ayudaMensaje,#ayudaMail{
color: black!important;
}
</style>
<script type="text/javascript">
    

     function enviar(){
   $.ajax({
        url: "http://formspree.io/informes@tecdemexico.edu.mx",
        method: "POST",
        data: {message: $('#ayudaMensaje').val(),
                mail:$('#ayudaMail').val()},
        dataType: "json"
    }).done(function(response){
        alert('Hemos recibido tu correo!');
        $('#ayudaMensaje').val('');
        $('#ayudaMail').val('');
        $('#ayuda').modal('toggle');
    }).fail(function() {
          alert( "Ocurrio un error, intenta después" );
        });
  }
  




function send(){
    $.ajax({
    url: "https://formspree.io/informes@tecdemexico.edu.mx", 
    method: "POST",
    data: {message: "hello!"},
    dataType: "json"
});
}



</script>
</html>